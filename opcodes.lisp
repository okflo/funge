(in-package :funge)

(defmacro defop ((thread-var
                  char function-name
                  list-of-valid-funges)
                 &body body)
  `(progn
     (defun ,function-name (,thread-var)
       (declare (ignorable ,thread-var))
       ,@body)
     (push (list ,(char-code char)
                 (function ,function-name)
                 (quote ,list-of-valid-funges))
           *available-operators*)))

(defun get-string-from-stack (thread)
  (with-output-to-string (s)
    (loop for i = (spop (toss thread))
       while (/= i 0)
       do
         (princ (code-char i) s))))

(defun push-string-to-stack (string thread)
  (push 0 (toss thread))
  (loop for i across (reverse string)
     do
       (push (char-code i) (toss thread))))

(defun get-vector-from-stack-relative-offs (thread)
  (reverse (loop for i in (reverse (storage-offset thread))
              collect (+ i (spop (toss thread))))))
(defun get-vector-from-stack-absolute (thread)
  (reverse (loop for i in (storage-offset thread)
              collect (spop (toss thread)))))

(defun push-vector-to-stack (vector thread)
  (loop for i in vector
     do
       (push i (toss thread))))


(defop (thread #\0 push-0 (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push 0 (toss thread)))
(defop (thread #\1 push-1 (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push 1 (toss thread)))
(defop (thread #\2 push-2 (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push 2 (toss thread)))
(defop (thread #\3 push-3 (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push 3 (toss thread)))
(defop (thread #\4 push-4 (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push 4 (toss thread)))
(defop (thread #\5 push-5 (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push 5 (toss thread)))
(defop (thread #\6 push-6 (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push 6 (toss thread)))
(defop (thread #\7 push-7 (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push 7 (toss thread)))
(defop (thread #\8 push-8 (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push 8 (toss thread)))
(defop (thread #\9 push-9 (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push 9 (toss thread)))
(defop (thread #\a push-10 (:befunge98 :trefunge98 :unefunge98))
  (push 10 (toss thread)))
(defop (thread #\b push-11 (:befunge98 :trefunge98 :unefunge98))
  (push 11 (toss thread)))
(defop (thread #\c push-12 (:befunge98 :trefunge98 :unefunge98))
  (push 12 (toss thread)))
(defop (thread #\d push-13 (:befunge98 :trefunge98 :unefunge98))
  (push 13 (toss thread)))
(defop (thread #\e push-14 (:befunge98 :trefunge98 :unefunge98))
  (push 14 (toss thread)))
(defop (thread #\f push-15 (:befunge98 :trefunge98 :unefunge98))
  (push 15 (toss thread)))

(defop (thread #\. output-integer (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (format t "~A " (spop (toss thread))))

(defop (thread #\, output-char (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (format t "~C" (code-char (spop (toss thread)))))

(defop (thread #\" toggle-string-mode (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (if (string-mode thread)
      (setf (string-mode thread) nil)
      (setf (string-mode thread) t)))

(defop (thread #\' fetch-character (:befunge98 :trefunge98 :unefunge98))
  (increment-instruction-pointer thread)
  (push (cell* (parent-funge-instance thread)
               (instruction-pointer thread))
        (toss thread)))

(defop (thread #\s store-character (:befunge98 :trefunge98 :unefunge98))
  (let ((char (spop (toss thread))))
    (increment-instruction-pointer thread)
    (setf (cell* (parent-funge-instance thread)
                 (instruction-pointer thread))
          char)))

(defop (thread #\: duplicate (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (let ((cell (spop (toss thread))))
    (push cell (toss thread))
    (push cell (toss thread))))

(defop (thread #\$ pop* (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (spop (toss thread)))

(defop (thread #\\ swap (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (let ((b (spop (toss thread)))
        (a (spop (toss thread))))
    (push b (toss thread))
    (push a (toss thread))))

(defop (thread #\n clear-stack (:befunge98 :trefunge98 :unefunge98))
  (setf (toss thread) nil))

(defop (thread #\{ begin-block (:befunge98 :trefunge98 :unefunge98))
  (let* ((n (spop (toss thread)))
         (cells-from-old-stack
          (loop for i from 1 to n
             collect (spop (toss thread)))))
    (when (< n 0)
      (loop for i from 1 to (abs n) do (push 0 (toss thread))))
    (push (make-instance 'stack) (stack-stack thread))
    (loop for i in (reverse cells-from-old-stack)
       do
         (push i (toss thread)))
    (loop for i in (storage-offset thread)  ;; reverse?
       do
         (push i (soss thread)))
    (setf (storage-offset thread)
          (add-delta-to-ip (delta thread) (instruction-pointer thread)))))

(defop (thread #\} end-block (:befunge98 :trefunge98 :unefunge98))
  (if (second (stack-stack thread))
      (let* ((n (spop (toss thread)))
             (cells-from-old-stack
              (loop for i from 1 to n
                 collect (spop (toss thread)))))
        (pop (stack-stack thread)) ;; check for non stack
        (setf (storage-offset thread)
              (loop for nil in (storage-offset thread)
                 collect (spop (toss thread))))
        (if (> n 0)
            (loop for i in (reverse cells-from-old-stack)
               do
                 (push i (toss thread)))
            (loop for i from 1 to (abs n)
               do (spop (toss thread)))))
      (reverse* thread)))

(defop (thread #\u stack-under-stack (:befunge98 :trefunge98 :unefunge98))
  (if (second (stack-stack thread))
      (let ((n (spop (toss thread))))
        (cond ((> n 0)
               (loop for i from 1 to n
                  do
                    (push (spop (soss thread)) (toss thread))))
              ((< n 0)
               (loop for i from 1 to (abs n)
                  do
                    (push (spop (toss thread)) (soss thread))))))
      (reverse* thread)))  

(defop (thread #\* multiplication (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push (* (spop (toss thread))
           (spop (toss thread)))
        (toss thread)))

(defop (thread #\+ addition (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push (+ (spop (toss thread))
           (spop (toss thread)))
        (toss thread)))

(defop (thread #\- substraction (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (let ((b (spop (toss thread)))
        (a (spop (toss thread))))
    (push (- a b) (toss thread))))

(defop (thread #\/ divide (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (let ((b (spop (toss thread)))
        (a (spop (toss thread))))
    (if (= b 0)
        (push 0 (toss thread))
        (push (floor (/ a b)) (toss thread)))))

(defop (thread #\% remainder (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (let ((b (spop (toss thread)))
        (a (spop (toss thread))))
    (if (= b 0)
        (push 0 (toss thread))
        (push (mod a b) (toss thread)))))

(defop (thread #\^ go-north (:befunge93 :befunge98 :trefunge98))
  (let ((new-delta (make-n-list (dimensions (parent-funge-instance thread)))))
    (setf (second new-delta) -1)
    (setf (delta thread) new-delta)))

(defop (thread #\v go-south (:befunge93 :befunge98 :trefunge98))
  (let ((new-delta (make-n-list (dimensions (parent-funge-instance thread)))))
    (setf (second new-delta) 1)
    (setf (delta thread) new-delta)))

(defop (thread #\< go-west (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (let ((new-delta (make-n-list (dimensions (parent-funge-instance thread)))))
    (setf (first new-delta) -1)
    (setf (delta thread) new-delta)))

(defop (thread #\> go-east (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (let ((new-delta (make-n-list (dimensions (parent-funge-instance thread)))))
    (setf (first new-delta) 1)
    (setf (delta thread) new-delta)))

(defop (thread #\? go-away (:befunge93 :befunge98 :trefunge98))
  ;; TODO: hmm, how should #\?  react in 1 dimension or more
  ;; interesting > 2 dimensions??
  (let ((new-delta (make-n-list (dimensions (parent-funge-instance thread)))))
    (setf (nth (random (dimensions (parent-funge-instance thread))) new-delta)
          (if (= 0 (random 2))
              -1
              1))
    (setf (delta thread) new-delta)))

(defop (thread #\h go-high (:trefunge98))
  (let ((new-delta (make-n-list (dimensions (parent-funge-instance thread)))))
    (setf (third new-delta) -1)
    (setf (delta thread) new-delta)))

(defop (thread #\l go-low (:trefunge98))
  (let ((new-delta (make-n-list (dimensions (parent-funge-instance thread)))))
    (setf (third new-delta) 1)
    (setf (delta thread) new-delta)))

(defop (thread #\] turn-right (:befunge98 :trefunge98))
  (let ((new-delta (make-n-list (dimensions (parent-funge-instance thread)))))
    (setf (first new-delta) (* (second (delta thread)) -1))
    (setf (second new-delta) (first (delta thread)))
    (setf (delta thread) new-delta)))

(defop (thread #\[ turn-left (:befunge98 :trefunge98))
  (let ((new-delta (make-n-list (dimensions (parent-funge-instance thread)))))
    (setf (first new-delta) (second (delta thread)))
    (setf (second new-delta) (* (first (delta thread)) -1))
    (setf (delta thread) new-delta)))

(defop (thread #\r reverse* (:befunge98 :trefunge98 :unefunge98))
  (setf (delta thread)
        (loop
           :for i :in (delta thread)
           :collect (* i -1))))

(defop (thread #\x set-delta (:befunge98 :trefunge98 :unefunge98))
  (let ((new-delta (nreverse
                    (loop for i from 1 to (dimensions (parent-funge-instance thread))
                       collect (spop (toss thread))))))
    (setf (delta thread) new-delta)))

(defop (thread #\z nop (:befunge98 :trefunge98 :unefunge98))
  )

(defop (thread #\Space space* (:befunge98 :trefunge98 :unefunge98))
  ;; This opcode should never be reached, as spaces are slurped by
  ;; ethereal-slurp - except: IP loops over a empty line, f.e. in an
  ;; empty (=without instructions) fungespace.
  )

(defop (thread #\# trampoline (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (increment-instruction-pointer thread))

(defop (thread #\j jump-over (:befunge98 :trefunge98 :unefunge98))
  (let ((steps (spop (toss thread))))
    (cond ((> steps 0)
           (loop
              :for i from 1 to steps
              :do
              (increment-instruction-pointer thread)))
          ((< steps 0)
           (setf (delta thread) (reverse-vec (delta thread)))
           (loop
              :for i from 1 to (abs steps)
              :do
              (increment-instruction-pointer thread))
           (setf (delta thread) (reverse-vec (delta thread))))
          (t 'nop))))

(defop (thread #\k iterate (:befunge98 :trefunge98 :unefunge98))
  (let ((bi (parent-funge-instance thread))
        (count-repeat (spop (toss thread)))
        (temp-ip (instruction-pointer thread)))
    (setf temp-ip (add-delta-to-ip (delta thread) temp-ip))
    (loop
       :named slurp-ethereal
       :do
       (cond ((= (cell* bi temp-ip) #. (char-code #\Space))
              (setf temp-ip (add-delta-to-ip (delta thread) temp-ip)))
             ((= (cell* bi temp-ip) #. (char-code #\;))
              (setf temp-ip (add-delta-to-ip (delta thread) temp-ip)))
             (t (return-from slurp-ethereal nil))))
    (if (= 0 count-repeat)
        (setf (instruction-pointer thread)
              temp-ip)
        (loop
           :for i :from 1 :to count-repeat
           :do
           (funcall (second (assoc (cell* bi temp-ip) (available-operators bi) :test #'=)) thread)))))

(defop (thread #\! logical-not (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (if (= 0 (spop (toss thread)))
      (push 1 (toss thread))
      (push 0 (toss thread))))

(defop (thread #\` greater-than (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (let ((b (spop (toss thread)))
        (a (spop (toss thread))))
    (if (> a b)
        (push 1 (toss thread))
        (push 0 (toss thread)))))

(defop (thread #\_ east-west-if (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (if (= (spop (toss thread)) 0)
      (go-east thread)
      (go-west thread)))

(defop (thread #\| north-south-if (:befunge93 :befunge98 :trefunge98))
  (if (= (spop (toss thread)) 0)
      (go-south thread)
      (go-north thread)))

(defop (thread #\m high-low-if (:trefunge98))
  (if (= (spop (toss thread)) 0)
      (go-low thread)
      (go-high thread)))

(defop (thread #\w compare (:befunge98 :trefunge98))
  (let ((b (spop (toss thread)))
        (a (spop (toss thread))))
    (cond ((< a b)
           (turn-left thread))
          ((> a b)
           (turn-right thread))
          (t 'nop))))

(defop (thread #\g get* (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push (cell* (parent-funge-instance thread)
               (get-vector-from-stack-relative-offs thread))
        (toss thread)))

(defop (thread #\p put* (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (setf (cell* (parent-funge-instance thread)
               (get-vector-from-stack-relative-offs thread))
        (spop (toss thread))))

(defop (thread #\& input-decimal (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push (parse-integer (read-line t) :junk-allowed t) (toss thread)))

(defop (thread #\~ input-character (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (push (char-code (read-char t)) (toss thread)))

(defop (thread #\t split (:concurrent))
  (push (duplicate-thread thread)
        (threads (parent-funge-instance thread))))

(defop (thread #\i input-file (:filesystem))
  (handler-case
      (let ((filename (get-string-from-stack thread)) 
            (flags (spop (toss thread)))
            (va (get-vector-from-stack-relative-offs thread)))
        (let ((vb
               (if (logbitp 0 flags)
                   ;; "least significant bit of flag is high, so we
                   ;; load in binary mode
                   (read-funge-binary (parent-funge-instance thread)
                                      filename
                                      va)
                   ;; otherwise load as normal...
                   (read-funge-native (parent-funge-instance thread)
                                      filename
                                      va))))
          (push-vector-to-stack vb thread)
          (push-vector-to-stack va thread)))
    (error ()
      ;; if something goes wrong, simply reflect
      (setf (delta thread)
            (reverse-vec (delta thread))))))

(defop (thread #\o output-file (:filesystem))
  (handler-case
      (let ((filename (get-string-from-stack thread)) 
            (flags (spop (toss thread)))
            (va (get-vector-from-stack-relative-offs thread))
            (vb (get-vector-from-stack-absolute thread)))
        (if (logbitp 0 flags)
            (write-funge-binary (parent-funge-instance thread)
                                filename
                                (+-vector (storage-offset thread)
                                          va)
                                (+-vector (storage-offset thread)
                                          va
                                          vb
                                          (make-n-list
                                           (dimensions (parent-funge-instance thread))
                                           -1)))
            (write-funge-native (parent-funge-instance thread)
                                filename
                                (+-vector (storage-offset thread)
                                          va)
                                (+-vector (storage-offset thread)
                                          va
                                          vb
                                          (make-n-list
                                           (dimensions (parent-funge-instance thread))
                                           -1)))))
    (error ()
      ;; if something goes wrong, simply reflect
      (setf (delta thread)
            (reverse-vec (delta thread))))))

(defun y-command-line-arguments ()
  (list "foo" "bar"))

(defun y-environment-variables ()
  #+sbcl
  (sb-ext:posix-environ)
  #+clisp
  (ext:getenv)) ;; TODO: CCL

(defmacro push-list-of-strings-on-toss (list-of-strings)
  `(progn
     (loop for i in ,list-of-strings
        do
          (loop for ii across i
             do
               (push (char-code ii) (toss thread))
               (incf no-push))
          (push 0 (toss thread))
          (incf no-push))
     (push 0 (toss thread))
     (incf no-push)))

(defop (thread #\= system-execution (:system-execution))
  (let ((sys-cmd (get-string-from-stack thread)))
    (uiop:run-program sys-cmd :output *standard-output*)))

(defop (thread #\y get-sysinfo (:befunge98 :trefunge98 :unefunge98))
  (recalc-borders (parent-funge-instance thread))
  (let* ((arg (spop (toss thread)))
         (no-of-stacks (length (stack-stack thread)))
         (stack-sizes (loop for i in (stack-stack thread)
                         collect (length (lifo i))))
         (time nil)
         (date nil)
         (least-point-vec (loop for i in (playfield-corners
                                          (parent-funge-instance thread))
                             collect (first i)))
         (greatest-point-vec (loop for i in (playfield-corners
                                             (parent-funge-instance thread))
                                for ii in least-point-vec
                                collect (+ (* ii -1) (second i))))
         (no-push 0))
    (multiple-value-bind (second minute hour
                                 date-day month year day daylight-p zone)
        (decode-universal-time
         (get-universal-time))
      (declare (ignore day daylight-p zone))
      (setf time (+ (* hour 256 256)
                    (* minute 256)
                    second))
      (setf date (+ (* (- year 1900) 256 256)
                    (* month 256)
                    (* date-day))))
    ;; 20. environment-variables
    (push 0 (toss thread))
    (incf no-push)
    (loop for i in (y-environment-variables)
       do
         (loop for ii across (reverse i)
            do
              (push (char-code ii) (toss thread))
              (incf no-push))
         (push 0 (toss thread))
         (incf no-push))
    ;; 19. command line arguments
    (push 0 (toss thread))
    (incf no-push)
    (push 0 (toss thread))
    (incf no-push)
    (loop for i in (y-command-line-arguments)
       do (loop for ii across i
             do (push (char-code ii) (toss thread))
               (incf no-push))
         (push 0 (toss thread))
         (incf no-push))
    ;; 18. size of stack-stack cells
    (loop for i in stack-sizes
       do
         (push i (toss thread))
         (incf no-push))
    ;; 17. total number of stacks
    (push no-of-stacks (toss thread))
    (incf no-push)
    ;; 16.  current (hour * 256 * 256) + (minute * 256) + (second) 
    (push time (toss thread))    
    (incf no-push)
    ;; 15. current ((year - 1900) * 256 * 256) + (month * 256) + (day of month) 
    (push date (toss thread))
    (incf no-push)
    ;; 14. greatest point which contains a non-space cell
    (loop for ii in greatest-point-vec
       do
         (push ii (toss thread))
         (incf no-push))
    ;; 13. least point which contains a non-space cell, relative to the origin
    (loop for ii in least-point-vec
       do
         (push ii (toss thread))
         (incf no-push))
    ;; 12. 
    (loop for i in (storage-offset thread)
       do
         (push i (toss thread))
         (incf no-push))
    ;; 11,
    (loop for i in (delta thread)
       do
         (push i (toss thread))
         (incf no-push))
    ;; 10. 
    (loop for i in (instruction-pointer thread)
       do
         (push i (toss thread))
         (incf no-push))
    ;; 09. team number (netfunge, beglad, ...)
    (push 0 (toss thread))
    (incf no-push)
    ;;08. thread id
    (push (id thread) (toss thread))
    (incf no-push)
    ;; 07. dimensions
    (push (dimensions (parent-funge-instance thread)) (toss thread))
    (incf no-push)
    ;; 06. path seperator
    (push (char-code #\/) (toss thread))
    (incf no-push)
    ;; 05. operating paradigm (shell)
    (if (find :system-execution
              (version (parent-funge-instance thread)))
        (push 1 (toss thread))
        (push 0 (toss thread)))
    (incf no-push)
    ;; 04 Version
    (push 1 (toss thread))
    (incf no-push)
    ;; 03 handprint
    (push 111107102108111 (toss thread))
    (incf no-push)
    ;; 02. bytes per cell
    (push 64 (toss thread))
    (incf no-push)
    ;; flags
    ;;(push (+ 1 2 4 8) (toss thread))
    (push (+ (if (find :concurrent
                       (version (parent-funge-instance thread)))
                 1 0)
             (if (find :filesystem
                       (version (parent-funge-instance thread)))
                 2 0)
             (if (find :filesystem
                       (version (parent-funge-instance thread)))
                 4 0)
             (if (find :system-execution
                       (version (parent-funge-instance thread)))
                 8 0))
          (toss thread))
    (incf no-push)
    (when (> arg 0)
      (let ((temp (nth (1- arg) (toss thread ))))
        (loop for i from 1 to no-push
           do
             (spop (toss thread)))
        (push temp (toss thread))))))

(defop (thread #\( load-semantics (:befunge98 :trefunge98 :unefunge98))
  (let* ((n (spop (toss thread)))
         (fingerprint (loop
                        with fingerprint = 0
                        for i from 1 to n
                        do
                           (setf fingerprint (+ (* fingerprint 256)
                                                (spop (toss thread))))
                        finally (return fingerprint))))
    (let ((fingerprint-implementation
            (find fingerprint *available-fingerprints* :key #'id)))
      (if fingerprint-implementation
          (progn
            (loop for (opcode func-name) in (instructions fingerprint-implementation)
                  do
                     (push func-name (second (assoc opcode (operating-fingerprints thread)))))
            (push fingerprint (toss thread))
            (push 1 (toss thread))
            (when (load-function fingerprint-implementation)
              (funcall (load-function fingerprint-implementation) thread fingerprint-implementation)))
          (setf (delta thread)
                (reverse-vec
                 (delta thread)))))))

(defop (thread #\) unload-semantics (:befunge98 :trefunge98 :unefunge98))
  (let* ((n (spop (toss thread)))
         (fingerprint (loop
                         with fingerprint = 0
                         for i from 1 to n
                         do
                           (setf fingerprint (+ (* fingerprint 256)
                                                (spop (toss thread))))
                         finally (return fingerprint))))
    (let ((fingerprint-implementation
           (find fingerprint *available-fingerprints* :key #'id)))
      (if fingerprint-implementation
          (loop for (opcode) in (instructions fingerprint-implementation)
             do
               (pop (second (assoc opcode (operating-fingerprints thread)))))
          (setf (delta thread)
                (reverse-vec (delta thread)))))))

(defop (thread #\@ end-thread (:befunge93 :befunge98 :trefunge98 :unefunge98))
  (format t "Finishing thread ~A~%" (id thread))
  (setf (threads (parent-funge-instance thread))
        (remove (id thread) (threads (parent-funge-instance thread))
                :key #'id))
  (unless (threads (parent-funge-instance thread))
    (funcall (what-todo-when-finished
              (parent-funge-instance thread))
             (parent-funge-instance thread))))

(defop (thread #\q quit (:befunge98 :trefunge98 :unefunge98))
  (setf (threads (parent-funge-instance thread)) nil)
  (funcall (what-todo-when-finished
            (parent-funge-instance thread))
           (parent-funge-instance thread)))





