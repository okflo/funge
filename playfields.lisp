(in-package :funge)

(defclass playfield ()
  ((funge-space :initarg :funge-space
                :reader funge-space
                :documentation "'Backlink' to the funge-space."))
  (:documentation "Motherclass of playfield-implementations."))

;; Playfield as adjustable array

(defparameter +initial-internal-array-size+ 1000)

(defclass playfield-array (playfield)
  ((raw-grid :accessor raw-grid)
   (internal-size :accessor internal-size))
  (:documentation "An implementation of a playfield, as an array,
  growing by 'adjust-array'. With each grow it doubles its size."))

(defmethod initialize-instance :after ((grid playfield-array) &key)
  (setf (raw-grid grid) (make-array (make-n-list (dimensions (funge-space grid))
                                                 +initial-internal-array-size+)
                                    :element-type 'integer
                                    :adjustable t
                                    :initial-element 32)
        (internal-size grid) (make-n-list (dimensions (funge-space grid))
                                          +initial-internal-array-size+)))

(defun de-normalize-coordinates (vector)
  "Encodes negative/positive coordinates into even/odd coordinates."
  (declare (optimize speed))
  (loop for i in vector
     collect
       (- (* (abs i) 2)
          (if (<= i 0) 0 1))))

(defun normalize-coordinates (vector)
  "Decodes even/odd coordinates into negative/positive coordinates."
  (declare (optimize speed))
  (loop for i in vector
     collect
       (if (oddp i)
           ;; ungerade
           (/ (1+ i) 2)
           ;; gerade
           (* (/ i 2) -1))))

(defmethod resize-funge-space-grid ((grid playfield-array))
  "Grows the size of the funge-space-array by doubling its size."
  (let ((new-internal-size (*-vector-skalar (internal-size grid) 2)))
    (setf (internal-size grid) new-internal-size)
    (setf (raw-grid grid)
          (adjust-array (raw-grid grid)
                        new-internal-size
                        :initial-element 32))))

(defmethod playfield-cell* ((grid playfield-array) coordinates)
  (declare (optimize speed))
  (loop
     for i of-type fixnum from 0 to (1- (the fixnum (dimensions (funge-space grid))))
     for borders in (playfield-corners (funge-space grid))
     do
       (when (< (the fixnum (nth i coordinates)) (the fixnum (first borders)))
         (return-from playfield-cell* 32))
       (when (> (the fixnum (nth i coordinates)) (the fixnum (second borders)))
         (return-from playfield-cell* 32)))
  (apply #'aref
         (cons (raw-grid grid)
               (de-normalize-coordinates coordinates))))

(defmethod (setf playfield-cell*) (value (grid playfield-array) coordinates)
  (loop for i from 0 to (1- (dimensions (funge-space grid)))
     for borders in (playfield-corners (funge-space grid))
     do
       (when (< (nth i coordinates) (first borders))
         (setf (first borders) (nth i coordinates)))
       (when (> (nth i coordinates) (second borders))
         (setf (second borders) (nth i coordinates))))
  (when (loop
           for icoord in (de-normalize-coordinates coordinates)
           for igrid in (internal-size grid)
           when (>= (1+ icoord) igrid)
           do
             (return t))
    (resize-funge-space-grid grid))
  (setf (apply #'aref (cons (raw-grid grid) (de-normalize-coordinates coordinates))) value))

(defmethod (setf playfield-cell) (value (grid playfield-array) &rest coordinates)
  (setf (cell* grid coordinates) value))

;; Playfield as hash-table

(defclass playfield-hash-table (playfield)
  ((playfield
    :accessor playfield
    :initform (make-hash-table :test 'equal :size #.(* 80 25))
    :documentation "A hash-table containing the actual cells of our
    playfield. The hashkey is a vector (represented as a list),
    hashvalue contains the value of the cell."))
  (:documentation "An implementation of a playfield, as a
  hash-table."))

(defmethod playfield-cell* ((fs playfield-hash-table) coordinates)
  "Gets the value of a cell by their COORDINATES. Defaults to
32 (#\Space). Coordinates provided as a list."
  (declare (optimize speed))
  (gethash coordinates (playfield fs) 32))

(defmethod (setf playfield-cell*) (val (bi playfield-hash-table) coordinates)
  "Sets the cell to VAL. Adjusts borders of the playfield. COORDINATES
provided as list."
  (declare (optimize speed)
           ((cons fixnum) coordinates))
  (unless (= (the fixnum (length coordinates))
             (dimensions (funge-space bi)))
    (error "Dimensions mismatch"))
  (loop for i from 0 to (1- (the fixnum (dimensions (funge-space bi))))
     for borders in (playfield-corners (funge-space bi))
     do
       (when (< (the fixnum (nth i coordinates)) (the fixnum (first borders)))
         (setf (first borders) (nth i coordinates)))
       (when (> (the fixnum (nth i coordinates)) (the fixnum (second borders)))
         (setf (second borders) (nth i coordinates))))
  (setf (gethash coordinates (playfield bi)) val))
