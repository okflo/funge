;; funge

;; Core Funge-Space. 

(in-package :funge)

(declaim (optimize (debug 3) (speed 0)))

(defvar *funge-version* "0.1"
  "Version of funge-machine.")

(defvar *developer-mode* nil
  "When t prints various information to *standard-output*.")

(defclass funge-space ()
  ((playfield-type
    :initform :hash-table
    :initarg :playfield-type
    :reader playfield-type
    :documentation "Type of playfield-implementation, currently either
    hash-table or auto-growing array.")
   (playfield
    :accessor playfield
    :documentation "Holds the playfield object.")
   (playfield-corners
    :initarg :playfield-corners
    :accessor playfield-corners
    :documentation "The playfield corners, n lists (as many as we have
    dimensions), with 2 elements: first representing \"least\" and
    second representing \"greater\" corner for each dimension.")
   (dimensions
    :initarg :dimensions
    :initform 2
    :reader dimensions
    :documentation "Number of dimensions."))
  (:documentation "Core funge-space."))

(defmethod initialize-instance :after ((fs funge-space) &key)
  "Initialize the playfield corners and initialize the playfield,
depending on 'playfield-type'."
  (setf (playfield-corners fs)
        (loop for i from 1 to (dimensions fs)
           :collect (list 0 0)))
  (cond ((equal (playfield-type fs) :hash-table)
         (setf (playfield fs) (make-instance 'playfield-hash-table :funge-space fs)))
        ((equal (playfield-type fs) :array)
         (setf (playfield fs) (make-instance 'playfield-array :funge-space fs)))))

(defclass funge-space-in-multiverse (funge-space)
  ((multiverse
    :initform nil
    :accessor multiverse
    :allocation :class)))

(defmethod initialize-instance :after ((fs funge-space-in-multiverse) &key)
  (push fs (multiverse fs)))

(defmethod kill-funge-space ((fs funge-space-in-multiverse))
  (setf (multiverse fs)
        (remove fs (multiverse fs) :test #'equal)))

(defmethod least-coord ((fs funge-space))
  "Returns the least coordinates of funge-space (most upper/left
corner in 2 dimensions) provided by the playfield corners."
  (loop for i in (playfield-corners
                  fs)
     collect (first i)))

(defmethod greatest-coord ((fs funge-space))
  "Returns the greatest coordinates of funge-space (most above/right
corner in 2 dimensions) provided by the playfield corners."
  (loop for i in (playfield-corners
                  fs)
     collect (second i)))

(defmethod cell* ((fs funge-space) coordinates-vec)
  "Get the value of a cell in the playfield."
  (playfield-cell* (playfield fs) coordinates-vec))

(defmethod (setf cell*) (value (fs funge-space) coordinates-vec)
  "Set the value of a cell in the playfield."
  (setf (playfield-cell* (playfield fs) coordinates-vec) value))

(defmethod map-funge-space ((fs funge-space)
                            function
                            &key
                              (least-corner (least-coord fs))
                              (greatest-corner (greatest-coord fs))
                              (ivec (copy-list least-corner))
                              (dimension (1- (dimensions fs))))
  "Maps over all cells of a FUNGE-SPACE calling FUNCTION on each
with the coordinates-vector of the current cell as parameter."
  (loop
     for i from (nth dimension least-corner) to (nth dimension greatest-corner)
     for iivec = (let ((tempvec (copy-list ivec)))
                   (setf (nth dimension tempvec) i)
                   tempvec)
     do
       (if (= dimension 0)
           (funcall function iivec)
           (map-funge-space
            fs function
            :least-corner least-corner
            :greatest-corner greatest-corner
            :ivec iivec
            :dimension (1- dimension)))))

(defmethod fill-funge-space ((fs funge-space)
                             char-code
                             &key
                               (least-corner (least-coord fs))
                               (greatest-corner (greatest-coord fs)))
  (map-funge-space fs
                   (lambda (vec)
                     (setf (cell* fs vec)
                           char-code))
                   :least-corner least-corner
                   :greatest-corner greatest-corner))

(defmethod map-funge-space-providing-dim ((fs funge-space) function
                                          &key
                                            (least-corner (least-coord fs))
                                            (greatest-corner (greatest-coord fs))
                                            (ivec (copy-list least-corner))
                                            (dimension (1- (dimensions fs))))
  "Maps over all cells of a FUNGE-SPACE calling FUNCTION on each
with the coordinates-vector of the current cell and the current
dimension as parameter."
  (loop
     for i from (nth dimension least-corner) to (nth dimension greatest-corner)
     for iivec = (let ((tempvec (copy-list ivec)))
                   (setf (nth dimension tempvec) i)
                   tempvec)
     do
       (when (/= dimension 0)
         (map-funge-space-providing-dim
          fs function
          :least-corner least-corner
          :greatest-corner greatest-corner
          :ivec iivec
          :dimension (1- dimension)))
       (funcall function iivec dimension)))

(defun copy-funge-space (source-funge target-funge
                         &key (source-least-cell (least-coord source-funge))
                           (source-greatest-cell (greatest-coord source-funge))
                           (target-offset (make-n-list (dimensions target-funge))))
  "Copy a funge space / part of a funge space."
  (if (/= (dimensions source-funge)
          (dimensions target-funge))
      (status-print "Dimension-missmatch")
      (map-funge-space source-funge
                       (lambda (vec)
                         (setf (cell* target-funge
                                      (+-vector
                                       (--vector vec source-least-cell)
                                       target-offset))
                               (cell* source-funge vec)))
                       :least-corner source-least-cell
                       :greatest-corner source-greatest-cell)))

(defun safe-copy-funge-space (source-funge target-funge
                              &key (source-least-cell (least-coord source-funge))
                                (source-greatest-cell (greatest-coord source-funge))
                                (target-offset (make-n-list (dimensions target-funge))))
  "Temporary buffers the copied space in a temporary funge-space."
  (let ((temp-funge-space (make-instance 'funge-space
                                         :dimensions (dimensions source-funge))))
    (copy-funge-space source-funge temp-funge-space
                      :source-least-cell source-least-cell
                      :source-greatest-cell source-greatest-cell)
    (copy-funge-space temp-funge-space target-funge
                      :target-offset target-offset)))

(defmethod recalc-borders ((fs funge-space))
  "Recalculates the borders of the playfield, giving them the most
narrow value. F.e. after spacing out some areas."
  (let ((new-borders
         (loop for i from 1 to (dimensions fs)
            :collect (list 0 0))))
    (map-funge-space
     fs
     (lambda (ivec)
       (loop ;; shouldn't we first check for 32?
          :for idim :from 0 :to (1- (dimensions fs))
          :do
          (when (/= (cell* fs ivec) 32)
            (when (< (nth idim ivec) (first (nth idim new-borders)))
              (setf (first (nth idim new-borders)) (nth idim ivec)))
            (when (> (nth idim ivec) (second (nth idim new-borders)))
              (setf (second (nth idim new-borders)) (nth idim ivec)))))))
    (setf (playfield-corners fs) new-borders)))

(defmethod in-space-p ((fs funge-space) vec)
  "Is VEC inside the borders of a FUNGE-SPACE playfield?"
  (loop
     for borders in (playfield-corners fs)
     for n = 0 then (incf n)
     do (unless (in-between (nth n vec) (first borders) (second borders))
          (return-from in-space-p nil)))
  t)


;;; IO Read/Write of complete a complete funge-space or parts of a
;;; funge-space.

;; TODO: what should we do, if a funge-space dimension is > 3?

(defun write-funge-binary (funge-instance file-name v-least v-greatest)
  "Write FUNGE-SPACE in 'binary-mode' - as defined by the specs for
the #\o command."
  (let ((output-buffer nil))
    (map-funge-space-providing-dim
     funge-instance 
     (lambda (icell dimension)
       (cond ((= dimension 0)
              (push (cell* funge-instance icell) output-buffer))
             ((= dimension 1)
              (loop ;; remove spaces by backtracking ...
                 while (= (car output-buffer) #.(char-code #\Space))
                 do (pop output-buffer))
              (push #.(char-code #\Return) output-buffer)
              (push #.(char-code #\Newline) output-buffer))
             ((= dimension 3)
              (push #.(char-code #\Page) output-buffer))))
     :least-corner v-least
     :greatest-corner v-greatest)
    (with-open-file (f file-name :direction :output
                       :if-exists :supersede
                       :if-does-not-exist :create
                       :external-format 
                       #+sbcl :latin1
                       #+ccl :ISO-8859-1
                       #+clisp (ext:make-encoding :charset 'charset:iso-8859-1
                                                  :line-terminator :dos)
                       #+ecl :iso-8859-1)
      (loop for i in
           (reverse output-buffer)
         do
           (write-char (code-char i) f)))))

(defun read-funge-binary (funge-instance file-name
                          &optional (offset (make-n-list (dimensions funge-instance))))
  "Reads a ASCII file following the specs for #\i in binary
mode. Returns the relative coordinates (to offset) of the greatest
written cell."
  (with-open-file (f file-name :direction :input :if-does-not-exist :error
                     :external-format
                     #+sbcl :latin1
                     #+ccl :ISO-8859-1
                     #+clisp (ext:make-encoding :charset 'charset:iso-8859-1
                                                :line-terminator :dos)
                     #+ecl :iso-8859-1)
    (let ((last-vector
           (loop
              with ioffset = (copy-list offset)
              with maxoffset = (make-n-list (dimensions funge-instance))
              for char = (read-char f nil nil)
              while char
              do
                (setf (cell* funge-instance (copy-list ioffset))
                      (char-code char))
                (incf (x ioffset))
                (loop
                   for i in ioffset
                   for n = 0 then (incf n)
                   do
                     (when (> i (nth n maxoffset))
                       (setf (nth n maxoffset) i)))
              finally (return maxoffset))))
      (loop
         for ilv in last-vector
         for ioffset in offset
         collect (- ilv ioffset)))))

(defun read-funge-native (funge-instance file-name
                          &optional (offset (make-n-list (dimensions funge-instance))))
  "Reads a ASCII file following the specs for #\i and the
sourcefile-format of befunge98. Returns the relative coordinates (to
offset) of the greatest written cell."
  (with-open-file (f file-name
                     :direction :input
                     :if-does-not-exist :error
                     :external-format
                     #+sbcl :utf8
                     #+mezzano :utf-8
                     #+ccl :ISO-8859-1
                     #+clisp (ext:make-encoding :charset 'charset:iso-8859-1
                                                :line-terminator :dos)
                     #+ecl :iso-8859-1)
    (let ((last-vector
            (loop
              with ioffset = (copy-list offset)
              with maxoffset = (make-n-list (dimensions funge-instance))
              for char = (read-char f nil nil)
              while char
              do
                 (cond
                   ((char= char #\Space )
                    ;; Space - do nothing, just step further
                    (incf (x ioffset)))
                   ((and (char= char #\Return)
                         (char= (peek-char nil f nil nil) #\Newline))
                    (when (> (dimensions funge-instance) 1)
                      (read-char f)
                      (setf (x ioffset) (x offset))
                      (incf (y ioffset))))
                   ((or (char= char #\Newline)
                        (char= char #\Return))
                    (when (> (dimensions funge-instance) 1)
                      (setf (x ioffset) (x offset))
                      (incf (y ioffset))))
                   ((char= char #\Page)
                    (when (> (dimensions funge-instance) 2)
                      (incf (z ioffset))
                      (setf (x ioffset) (x offset))
                      (setf (y ioffset) (y offset))))
                   (t
                    (setf (cell* funge-instance (copy-list ioffset))
                          (char-code char))
                    (incf (x ioffset))))
                 (loop
                   for i in ioffset
                   for n = 0 then (incf n)
                   do
                      (when (> i (nth n maxoffset))
                        (setf (nth n maxoffset) i)))
              finally (return maxoffset))))
      (loop
        for ilv in last-vector
        for ioffset in offset
        collect (- ilv ioffset)))))

(defun %write-funge (f funge-instance absolute-least-point absolute-greatest-point
                     &optional
                       (ivec (copy-list absolute-least-point))
                       (dimension (1- (dimensions funge-instance))))
  ;; TODO rewrite to absolute coordinates und handle relative absolute-greatest-point in calling functions
  (loop
     for i from (nth dimension absolute-least-point) to (nth dimension absolute-greatest-point)
     for iivec = (let ((tempvec (copy-list ivec)))
                   (setf (nth dimension tempvec) i)
                   tempvec)
     do
       (cond
         ((= dimension 0)
          (write-char (code-char (cell* funge-instance iivec)) f))
         ((= dimension 1)
          (%write-funge f funge-instance absolute-least-point absolute-greatest-point iivec (1- dimension))
          ;;(write-char #\Return f) ;; removed to pass Mycology-Test-Suite...
          (write-char #\Newline f))
         ((= dimension 2)
          (%write-funge  f funge-instance absolute-least-point absolute-greatest-point iivec (1- dimension))
          (write-char #\Page f))
         (t
          (%write-funge  funge-instance f absolute-least-point absolute-greatest-point iivec (1- dimension))
          (format t "not-defined ~%")))))

(defun write-funge-native (funge-instance file-name
                           absolute-least-point absolute-greatest-point)
  "Write a funge-space (or parts of it) to an ASCII file following the
specs."
  (with-open-file (f file-name :direction :output
                               :if-exists :supersede
                               :if-does-not-exist :create
                               :external-format 
                                          #+sbcl :utf8
                               #+ccl :ISO-8859-1
                               #+clisp (ext:make-encoding :charset 'charset:iso-8859-1
                                                          :line-terminator :dos)
                               #+ecl :iso-8859-1)
    (%write-funge f funge-instance absolute-least-point absolute-greatest-point)))






