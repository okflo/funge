(in-package :funge)

(defvar *available-operators* nil)

(defclass funge-instance (funge-space)
  ((threads
    :accessor threads
    :initform nil
    :documentation "List of running threads.")
   (thread-counter
    :accessor thread-counter
    :initform 0
    :documentation "Counter to generate the ID of next initiated
    thread.")
   (no-of-ticks
    :accessor no-of-ticks
    :initform 0
    :documentation "Counts the ticks of a running funge-instance.")
   (available-operators
    :accessor available-operators
    :documentation "List of functions representing the opcodes of this
    funge-instance. Format: '(char function
    list-of-valid-funge-types).")
   (available-operators*
    :accessor available-operators*
    :initform (make-array 256)
    :documentation "Array of ptr to functions. Index is the code-char
    of the opcode.")
   (current-directory
    :initarg :current-directory
    :accessor current-directory
    :documentation "Current directory of i/o operations. Defaults to
    *default-pathname-defaults*."
    :initform *default-pathname-defaults*)
   (version
    :initarg :version
    :initform (list :befunge98 :concurrent :filesystem)
    :reader version
    :documentation "Types (f.e. :befunge98) and
    options (f.e. :concurrent) of funge-instance as keyword in a
    list")
   (what-todo-when-finished
    :initarg :what-todo-when-finished
    :accessor what-todo-when-finished
    :initform (lambda (self)
                (declare (ignorable self))
                (format t "Last thread finished."))
    :documentation "This function will be called, when the last thread
   of that instance finishes."))
  (:documentation "A funge-instance is a funge-space, threads and the
  corresponding opcodes."))

(defmethod initialize-instance :after ((bi funge-instance) &key) 
  ;; provide instance with operators
  (setf (available-operators bi)
        (loop for iop in *available-operators*
           when (loop 
                   for ivers in (version bi) 
                   when (find ivers (third iop))
                   return t)
           collect iop))
  ;; put the available opcodes into array
  (loop for iop in *available-operators*
     when (loop 
             for ivers in (version bi) 
             when (find ivers (third iop))
             return t)
     do
       (setf (aref (available-operators* bi) (first iop))
             (second iop)))
  ;; initiate first thread
  (push (make-instance 'thread :parent-funge-instance bi)
        (threads bi)))

(defmethod get-next-thread-id ((bi funge-instance))
  "Receive the next ID of a new thread inside a FUNGE-INSTANCE."
  (prog1 (thread-counter bi)
    (incf (thread-counter bi))))

(defclass thread ()
  ((parent-funge-instance
    :reader parent-funge-instance
    :initarg :parent-funge-instance
    :documentation "Parent funge-instance of this thread.")
   (id
    :reader id
    :documentation "Thread ID")
   (instruction-pointer
    :initarg :instruction-pointer
    :accessor instruction-pointer
    :documentation "Vector representing the IP.")
   (storage-offset
    :accessor storage-offset
    :documentation "The storage-offset (gets added to each ops
    regarding absolute vectors).")
   (delta
    :initarg :delta
    :accessor delta
    :documentation "The delta gets added to the IP to get the next
    evaluated cell.")
   (stack-stack
    :initarg :stack-stack
    :accessor stack-stack
    :initform (list (make-instance 'stack))
    :documentation "List of stacks.")
   (string-mode
    :accessor string-mode
    :initform nil
    :documentation "Flag that indicates whether this thread is in
    string-mode (T).")
   (operating-fingerprints
    :accessor operating-fingerprints
    :initform (loop for i across "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                    collect (list (char-code i) (list)))
    :documentation "A list of fingerprints currently active. Format:
    '((char-code function-stack) ...)")
   (properties
    :accessor properties
    :initform nil
    :documentation "A key-list of properties, that can be
    set/retrieved (f.e. for flags by fingerprints per thread."))
  (:documentation "A thread existing inside a funge-space."))

(defmethod initialize-instance ((th thread) &key)
  (call-next-method)
  (setf (slot-value th 'id) (get-next-thread-id (parent-funge-instance th))
        (instruction-pointer th) (make-n-list (dimensions (parent-funge-instance th)))
        (storage-offset th) (make-n-list (dimensions (parent-funge-instance th)))
        (delta th) (let ((delta (make-n-list (dimensions (parent-funge-instance th)))))
                     (setf (x delta) 1)
                     delta)))

(defmethod duplicate-thread ((th-mother thread))
  "Duplicates the THREAD including its properties. IP is set to the
reverse of the IP of the mother-thread."
  (let ((child-thread (make-instance 'thread
                                     :parent-funge-instance (parent-funge-instance th-mother))))
    (setf (instruction-pointer child-thread) (instruction-pointer th-mother)
          (storage-offset child-thread) (storage-offset th-mother)
          (delta child-thread) (reverse-vec (delta th-mother))
          (stack-stack child-thread)
          (loop for istack in (stack-stack th-mother)
             collect
               (copy-stack istack)))
    (increment-instruction-pointer child-thread)
    child-thread))

(defmethod toss ((th thread))
  "Shortcut to the TOSS (top of stack stack) of THREAD."
  (lifo (car (stack-stack th))))

(defmethod (setf toss) (value (th thread))
  "Access the TOSS (top of stack stack)."
  (setf (lifo (car (stack-stack th))) value))

(defmethod soss ((th thread))
  "Shortcut to the SOSS (second of stack stack)."
  (lifo (second (stack-stack th))))

(defmethod (setf soss) (value (th thread))
  "Access the SOSS (second of stack stack)."
  (setf (lifo (second (stack-stack th))) value))

(defmacro spop (place)
  "Safe pop - when stack is empty it returns 0."
  (let ((var (gensym)))
    `(let ((,var (pop ,place)))
       (if ,var
           ,var
           0))))

(defun add-delta-to-ip (delta ip)
  "Adds DELTA the the IP."
  (loop
     for ide in delta
     for iip in ip
     :collect (+ ide iip)))

(defmethod increment-instruction-pointer ((th thread))
  "Increments the IP of the THREAD by delta. Wraps to the other edge
if it reaches 'eternal space'."
  (setf (instruction-pointer th)
        (add-delta-to-ip (delta th) (instruction-pointer th)))
  (when (not (in-space-p (parent-funge-instance th) (instruction-pointer th)))
    (let ((ip (instruction-pointer th))
          (rev-delta (reverse-vec (delta th))))
      (loop for i = (add-delta-to-ip rev-delta ip)
         while (in-space-p (parent-funge-instance th) i)
         do
           (setf ip (add-delta-to-ip rev-delta ip)))
      (setf (instruction-pointer th) ip))))

(defclass stack ()
  ((lifo
    :accessor lifo
    :initarg :lifo
    :initform nil
    :documentation "LIFO of stack.")))

(defmethod copy-stack ((mother-stack stack))
  "Copies the entire STACK."
  (make-instance 'stack
                 :lifo (copy-list (lifo mother-stack))))

(defmethod tick ((bi funge-instance))
  "All existing threads in a FUNGE-INSTANCE make a tick."
  (incf (no-of-ticks bi))
  (loop for thread in (threads bi)
     do
       (when *developer-mode*
         (format t "tick: ~A thread: ~A ip: ~A delta ~A~%"
                 (no-of-ticks bi) (id thread)
                 (instruction-pointer thread) (delta thread)))
       (let ((*default-pathname-defaults* (current-directory bi)))
         (tick thread))))

(defmethod tick ((th thread))
  "Executes a tick on the current THREAD. Handles instructions, slurps
Spaces, string-mode and SGML-Spaces. warp over #\;-instructions."
  (let* ((bi (parent-funge-instance th))
         (cur (cell* bi (instruction-pointer th))))
    ;;check for string-mode
    (if (string-mode th)
        (progn
          (when (not (in-space-p bi (instruction-pointer th)))
            (format t "Cur:~A IP:~A~%" cur (instruction-pointer th)))
          (cond ((= cur #.(char-code #\"))
                 ;; end string-mode
                 (setf (string-mode th) nil))
                ((= cur #.(char-code #\Space))
                 (push cur (toss th))
                 (loop for peek = (cell* bi
                                         (add-delta-to-ip (delta th)
                                                          (instruction-pointer th)))
                    while (and (= peek #. (char-code #\Space))
                               (in-space-p bi
                                           (add-delta-to-ip
                                            (delta th)
                                            (instruction-pointer th))))
                    do
                      (increment-instruction-pointer th)))
                (t
                 (push cur (toss th)))))
        (progn
          (loop named slurp-ethereal
             do
               (cond ((= (cell* bi (instruction-pointer th))
                         #.(char-code #\Space))
                      (increment-instruction-pointer th)
                      (let ((temp-ip (instruction-pointer th)))
                        (loop
                           for peek = (cell* bi (instruction-pointer th))
                           while (= peek #. (char-code #\Space))
                           do
                             (increment-instruction-pointer th)
                             (when (equal (instruction-pointer th)
                                          temp-ip)
                               (return-from slurp-ethereal nil)))))
                     ((= (cell* bi (instruction-pointer th))
                         #.(char-code #\;))
                      (increment-instruction-pointer th)
                      (loop
                         for peek = (cell* bi (instruction-pointer th))
                         while (/= peek #. (char-code #\;))
                         do (increment-instruction-pointer th))
                      (increment-instruction-pointer th))
                     (t (return-from slurp-ethereal nil))))
          (setf cur (cell* bi (instruction-pointer th)))
          ;; finally execute opcode
          (when *developer-mode*
            (format t "cur: ~A/~C op: ~A stack: ~A~%" cur (code-char cur) (second (assoc cur (available-operators bi) :test #'=)) (toss th)))
          (let ((func-to-call
                 (or
                  (find-fingerprint-opcode th cur)
                  (second
                   (assoc cur
                          (available-operators bi)
                          :test #'=))
                  ;; (aref (available-operators* bi) cur)
                  )))
            (if func-to-call
                (funcall func-to-call th)
                (progn
                  (format t "Unknown opcode: ~A (~C)~%" cur (code-char cur))
                  (setf (delta th)
                        (loop
                           :for i :in (delta th)
                           :collect (* i -1))))))))
    (increment-instruction-pointer th)))

(defmethod run ((bi funge-instance))
  "Runs the FUNGE-instance while there are active threads."
  (loop
     :while (threads bi)
     :do (tick bi)))

(defun start (file &key (current-directory cl:*default-pathname-defaults*))
  (let ((funge-instance (make-instance 'funge-instance
                                       :version '(:befunge98 :concurrent :filesystem)
                                       :current-directory current-directory)))
    (read-funge-native funge-instance file)
    (run funge-instance)))













