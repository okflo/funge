;; Funge

;; Mingle-Mangle of various functions to handle vectors (representet
;; als lists).

(in-package :funge)

(defmacro x (vector)
  "Get first skalar of a vector."
  `(first ,vector))
(defmacro y (vector)
  "Get second skalar of a vector."
  `(second ,vector))
(defmacro z (vector)
  "get third skalar of a vector."
  `(third ,vector))

(defun in-between (val l r)
  "Is val between l and r?"
  (when (and (>= val l)
             (<= val r))
    t))

(defun reverse-vec (vec)
  "Reverses a vector, f.e. the delta after hitting #\r."
  (loop for i in vec
     collect (* i -1)))

(defun make-n-list (n &optional (initial-value 0))
  "Make an n-dimension list, optional with initial-value (defaults to
0)."
  (loop for i from 1 to n collect initial-value))

(defun +-vector (&rest rest)
  "Summate all vectors. The result-vector is as big as the biggest one
of REST."
  (let ((result (make-n-list
                 (apply #'max (the (cons fixnum) (mapcar #'length rest))))))
    (loop for i in rest
       do
         (loop
            for ii in i
            for n = 0 then (incf n)
            do
              (setf (nth n result)
                    (+ (nth n result) ii))))
    result))

(defun +-nvector (&rest rest)
  "Summate all vectors. The result-vector is as big as the biggest one
of REST. Destructive - first vector will be used as result."
  (let ((result (first rest)))
    (loop for i in (cdr rest)
       do
         (loop
            for ii in i
            for n = 0 then (incf n)
            do
              (setf (nth n result)
                    (+ (nth n result) ii))))
    result))

(defun --vector (&rest rest)
  "Subtract all vectors. "
  (let ((result (make-n-list
                 (apply #'max (mapcar #'length rest)))))
    (setf result (+-vector result (first rest)))
    (loop for i in (cdr rest)
       do
         (loop
            for ii in i
            for n = 0 then (incf n)
            do
              (setf (nth n result) (- (nth n result) ii))))
    result))

(defun --nvector (&rest rest)
  "Subtract all vectors. "
  (let ((result (first rest)))
    (loop for i in (cdr rest)
       do
         (loop
            for ii in i
            for n = 0 then (incf n)
            do
              (setf (nth n result) (- (nth n result) ii))))
    result))

(defun *-vector-skalar (vector skalar)
  "Multiplies a vector with a skalar"
  (loop for i in vector collect (* i skalar)))









