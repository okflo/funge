(in-package :funge)

(defvar *available-fingerprints* nil)

(defclass fingerprint ()
  ((name
    :initarg :name
    :reader name
    :documentation "Name of the fingerprint.")
   (id
    :reader id
    :documentation "Calculated fingerprint id regarding the specs.")
   (load-function
    :initarg :load-function
    :reader load-function
    :initform nil
    :documentation "If set, a function, that is called when the
    fingerprint is activated.")
   (unload-function
    :initarg :unload-function
    :reader unload-function
    :initform nil
    :documentation "If set, a function, that is called when the
    fingerprint is unloaded.")
   (instructions
    :accessor instructions
    :initform nil
    :documentation "A alist of the opcodes and their function
    name.")
   (overloaded-opcodes
    :accessor overloaded-opcodes
    :initform nil
    :documentation "An alist of 'normal' opcodes (and the
    function-name) that overloads the regular opcodes. When the
    fingerprint is active, these opcodes supersede the regular
    opcodes.")))

(defun calc-fingerprint-id (string)
  (let ((val 0))
    (loop for i across string
       do
         (setf val (+ (* val 256) (char-code i))))
    val))

(defmethod initialize-instance :after ((f fingerprint) &key)
  (setf (slot-value f 'id) (calc-fingerprint-id (name f))))

(defmacro def-fingerprint ((name list-of-versions &key load-function unload-function)
                           &body list-of-opcodes-and-their-implementation)
  `(progn
     (let ((fingerprint (make-instance 'fingerprint
                                       :name ,name
                                       :load-function ,load-function
                                       :unload-function ,unload-function)))
       ,@(loop for iopcodedef in list-of-opcodes-and-their-implementation
            collect
              (let ((func-name (intern (format nil "~A-~A" name (symbol-name (second iopcodedef))))))
                `(progn
                   (defun ,func-name (thread)
                     (declare (ignorable thread))
                     ,@ (nthcdr 2 iopcodedef))
                   (push (list ,(char-code (first iopcodedef)) ',func-name ',list-of-versions) (instructions fingerprint)))))
       (push fingerprint *available-fingerprints*))))

(defmethod find-fingerprint-opcode ((thread thread) opcode)
  (car (second
        (assoc opcode
               (operating-fingerprints thread)))))

;;; Fingerprint 0x4e554c4c (Null)
(def-fingerprint ("NULL" (:befunge98)
                         :load-function ()
                         :unload-function ())
  ;; https://github.com/catseye/Funge-98/blob/master/library/NULL.markdown
  (#\A A-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\B B-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\C C-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\D C-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\E E-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\F F-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\G G-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\H H-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\I I-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\J J-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\K K-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\L L-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\M M-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\N N-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\O O-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\P P-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\Q Q-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\R R-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\S S-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\T T-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\U U-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\V V-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\W W-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\X X-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\Y Y-rev
       (setf (delta thread)
             (reverse-vec (delta thread))))
  (#\Z Z-rev
       (setf (delta thread)
             (reverse-vec (delta thread)))))

;;; Fingerprint 0x524f4d41 ('ROMA')
(def-fingerprint ("ROMA" (:befunge98)
                         :load-function ()
                         :unload-function ())
  ;; https://github.com/catseye/Funge-98/blob/master/library/ROMA.markdown
  (#\C push-100
       (push 100 (toss thread)))
  (#\D push-500
       (push 500 (toss thread)))
  (#\I push-1
       (push 1 (toss thread)))
  (#\L push-50
       (push 50 (toss thread)))
  (#\M push-1000
       (push 1000 (toss thread)))
  (#\V push-5
       (push 5 (toss thread)))
  (#\X push-10
       (push 10 (toss thread))))

;;; Fingerprint 0x52454643 ('REFC')
(defparameter *REFC-counter* 0)
(defparameter *REFC-alist* nil)
(def-fingerprint ("REFC" (:befunge98)
                         :load-function ()
                         :unload-function ())
  (#\R reference-vector
       (let ((vector (loop for i from 1 to (dimensions (parent-funge-instance thread))
                        collect (spop (toss thread))))
             (reference (incf *REFC-counter*)))
         (push (list reference vector) *REFC-alist*)
         (push reference (toss thread))))
  (#\D dereference-vector
       (let ((reference (spop (toss thread))))
         (loop for i in (reverse (second (assoc reference *REFC-alist* :test #'=)))
            do
              (push i (toss thread))))))

;;; Fingerprint 0x4d4f4455 ('MODU')
(def-fingerprint ("MODU" (:befunge98)
                         :load-function ()
                         :unload-function ())
    ;; https://github.com/catseye/Funge-98/blob/master/library/MODU.markdown
    ;; modulus vs. remainder... This fingerprint is bad specified..
    (#\M signed-result-modulo
         ;; identical to mod
         (let ((b (spop (toss thread)))
               (a (spop (toss thread))))
           (if (= b 0)
               (push 0 (toss thread))
               (push (mod a b) (toss thread)))))
  (#\U Sam-Holdens-unsigned-result-modulo
       ;; Who the fuck is Sam Holden? :)
       (let ((b (spop (toss thread)))
             (a (spop (toss thread))))
         (if (= b 0)
             (push 0 (toss thread))
             (push (abs (mod a b)) (toss thread)))))
  (#\R C-language-integer-remainder
       ;; identical to rem
       (let ((b (spop (toss thread)))
             (a (spop (toss thread))))
         (if (= b 0)
             (push 0 (toss thread))
             (push (rem a b) (toss thread))))))

;; Fingerprint 0x53554252 SUBR
(def-fingerprint ("SUBR" (:befunge98)
                  :load-function (lambda (thread fingerprint)
                                   (declare (ignorable thread fingerprint))
                                   (log:debug "Fingerprint initialised.")
                                   (setf (getf (properties thread) :subr-absolute-mode) t)))
  (#\A set-absolute-mode
       (setf (getf (properties thread) :subr-absolute-mode) t))
  (#\O set-relative-mode
       (setf (getf (properties thread) :subr-absolute-mode) nil))
  (#\C call-subroutine
       (let* ((n (spop (toss thread)))
              (target (if (getf (properties thread) :subr-absolute-mode)
                          (get-vector-from-stack-absolute thread)
                          (+-vector (get-vector-from-stack-absolute thread)
                                    (instruction-pointer thread))))
              (buffer-stack (reverse (loop for i from 1 to n collect (spop (toss thread))))))
         (push-vector-to-stack (instruction-pointer thread) thread)
         (push-vector-to-stack (delta thread) thread)
         (setf (delta thread)  (let ((delta (make-n-list (dimensions (parent-funge-instance thread)))))
                                 (setf (x delta) 1)
                                 delta))
         (setf (instruction-pointer thread) (--vector target (delta thread)))
         (loop for i in buffer-stack do (push (pop buffer-stack) (toss thread)))))
  (#\R subroutine-return
       (let* ((n (spop (toss thread)))
              (buffer-stack (reverse (loop for i from 1 to n collect (spop (toss thread)))))
              (delta (get-vector-from-stack-absolute thread))
              (target (if (getf (properties thread) :subr-absolute-mode)
                          (get-vector-from-stack-absolute thread)
                          (+-vector (get-vector-from-stack-absolute thread)
                                    (instruction-pointer thread)))))
         (setf (delta thread) delta)
         (setf (instruction-pointer thread) target)
         (loop for i in buffer-stack do (push (pop buffer-stack) (toss thread)))))
  (#\J jump
       (setf (delta thread)  (let ((delta (make-n-list (dimensions (parent-funge-instance thread)))))
                               (setf (x delta) 1)
                               delta))
       (setf (instruction-pointer thread)
             (--vector (+-vector (get-vector-from-stack-absolute thread)
                                 (instruction-pointer thread))
                       (delta thread)))))

;;;"STRN" 0x5354524E
(def-fingerprint ("STRN" (:befunge98)
                  :load-function ()
                  :unload-function ())
  ;; http://www.rcfunge98.com/rcsfingers.html#STRN
  ;; https://pythonhosted.org/PyFunge/fingerprint/STRN.html
  (#\A append
       (let ((s1 (get-string-from-stack thread))
             (s2 (get-string-from-stack thread)))
         (push-string-to-stack (concatenate 'string s1 s2) thread)))
  (#\C compare
       (let ((s1 (get-string-from-stack thread))
             (s2 (get-string-from-stack thread)))
         (cond
           ((string= s1 s2)
            (push 0 (toss thread)))
           ((string< s1 s2)
            (push -1 (toss thread)))
           ((string> s1 s2)
            (push 1 (toss thread))))))
  (#\D print
       (princ (get-string-from-stack thread)))
  (#\F search
       (let* ((s1 (get-string-from-stack thread))
              (s2 (get-string-from-stack thread))
              (pos (search s2 s1)))
         (if pos
             (push-string-to-stack (subseq s1 pos) thread)
             (push 0 (toss thread)))))
  (#\G get-string-from-funge-space
       (let ((str-pos (get-vector-from-stack-relative-offs thread))
             (delta (make-n-list
                     (dimensions (parent-funge-instance thread))))
             (result nil))
         (setf (x delta) 1)
         (push 0 (toss thread))
         (loop
           for vpos = str-pos
             then (setf vpos (+-vector vpos delta))
           until (or (= (cell* (parent-funge-instance thread) vpos) 0)
                     (> (x vpos)
                        (x (greatest-coord
                            (parent-funge-instance thread)))))
           do (push (cell* (parent-funge-instance thread) vpos) result))
         (loop for i in result
               do (push i (toss thread)))))
  (#\P put-string-to-funge-space
       (let ((ppos (get-vector-from-stack-relative-offs thread))
             (str (get-string-from-stack thread))
             (delta (make-n-list
                     (dimensions (parent-funge-instance thread)))))
         (setf (x delta) 1)
         (loop
           for pos = ppos then (setf pos (+-vector pos delta))
           for c across str
           do (setf (cell* (parent-funge-instance thread)
                           pos)
                    (char-code c))
           finally
              (setf (cell* (parent-funge-instance thread)
                           pos)
                    0))))
  (#\L leftmost-n-string
       (let ((n (abs (spop (toss thread))))
             (s (get-string-from-stack thread)))
         (push-string-to-stack (subseq s 0 n) thread)))
  (#\R rightmost-n-string
       (let ((n (abs (spop (toss thread))))
             (s (get-string-from-stack thread)))
         (push-string-to-stack (subseq s (- (length s) n)) thread)))
  (#\M n-characters-starting-at-position-s
       (let ((n (abs (spop (toss thread))))
             (s (abs (spop (toss thread))))
             (str (get-string-from-stack thread)))
         (push-string-to-stack
          (with-output-to-string (stream)
            (loop
              for is across str
              for in = 0 then (incf in)
              when (and (>= in s)
                        (< in (+ s n)))
                do (princ is stream)))
          thread)))
  (#\N length
       (let ((s (get-string-from-stack thread)))
         (push-string-to-stack s thread)
         (push (length s)
               (toss thread))))
  (#\S n-to-string
       (push-string-to-stack
        (format nil "~S" (spop (toss thread))) thread))
  (#\V parse-string
       (let ((str (get-string-from-stack thread)))
         (handler-case
             (push (parse-integer str) (toss thread))
           (error () nil)))))
#+nil(
;;; "SOCKi" 0x534F434B
      (defparameter *socket-next-id* 0)
      (defparameter *socket-list* nil "List of SOCK-socket objects.")

      (defun SOCK-calculate-int-ip-from-vec-ip (vec)
        (let ((result 0))
          (loop
             for i across (nreverse vec)
             for n = 1 then (setf n (* n 256))
             do (setf result (+ result (* i n))))
          result))

      (defclass SOCK-socket ()
        ((socket-id
          :accessor socket-id :initarg :socket-id
          :documentation "Internal pseudo-fd to access the socket")
         (local-addr
          :accessor local-addr :initarg :local-addr)
         (local-port
          :accessor local-port :initarg :local-port)
         (remote-addr
          :accessor remote-addr :initarg :remote-addr)
         (remote-port
          :accessor remote-port :initarg :remote-port)
         (addr-family
          :accessor addr-family :initarg :addr-family
          :documentation "Broken, should be always 'AF_INET' (2).")
         (protocol-family
          :accessor protocol-family :initarg :protocol-family
          :documentation "Broken, should be always 'PF_INET' (2).")
         (socket-option
          :accessor socket-option :initarg :socket-option
          :documentation "We only honor SO_REUSEADDR (2),
other valid but ignored are: 1=SO_DEBUG, 3=SO_KEEPALIVE,
4=SO_DONTROUTE, 5=SO_BROADCAST, 6=OOBINLINE")
         (socket-type
          :accessor socket-type :initarg :socket-type
          :documentation "1=SOCK_DGRAM, 2=SOCK_STREAM")
         (protocol
          :accessor protocol :initarg :protocol
          :documentation "1=tcp, 2=udp")
         (usocket
          :accessor usocket
          :documentation "The raw socket from usocket.")))

      (defun make-socket (&key
                            (protocol :tcp)
                            (type :stream)
                            (family :inet))
        (let ((socket (make-instance 'SOCK-socket
                                     :socket-id (incf *socket-next-id*)
                                     :addr-family family
                                     :protocol-family protocol
                                     :socket-type type)))
          (push socket *socket-list*)
          (socket-id socket)))

      (defun get-socket-from-socket-list (socket-id)
        (or (find socket-id *socket-list* :key #'socket-id :test #'=)
            (error "No socket-descriptor with socket-id ~A" socket-id)))

      (defun remove-socket-from-socket-list (socket-id)
        (usocket:socket-close
         (usocket (get-socket-from-socket-list socket-id)))
        (setf *socket-list*
              (remove socket-id *socket-list* :key #'socket-id :test #'=)))

(def-fingerprint ("SOCK" (:befunge98)
                         :load-function ()
                         :unload-function ())
  ;; http://www.rcfunge98.com/rcsfingers.html#SOCK
  ;; https://pythonhosted.org/PyFunge/fingerprint/SOCK.html
  (#\S create-socket
       (let ((protocol (spop (toss thread)))
             (type (spop (toss thread)))
             (family (spop (toss thread))))
         (cond
           ((= protocol 1)
            (setf protocol :tcp))
           ((= protocol 2)
            (setf protocol :udp))
           (t (error "unknown protocol")))
         (cond
           ((= type 1)
            (setf type :datagram))
           ((= type 2)
            (setf type :stream))
           (t (error "unknown protocol type")))
         (cond
           ((= family 1)
            (error "not implemented"))
           ((= family 2)
            (setf family :inet))
           (t (error "unknown protocol family")))
         (push (make-socket :protocol protocol :type type :family family)
               (toss thread))))
  (#\A accept-connection
       (let* ((socket-id (spop (toss thread)))
              (socket (get-socket-from-socket-list socket-id))
              (new-socket (usocket:socket-accept (usocket socket)))
              (new-socket-obj (get-socket-from-socket-list
                               (make-socket :protocol (protocol-family socket)
                                            :type (socket-type socket)
                                            :family (addr-family socket))))
              (peer-addr (usocket:get-peer-address new-socket))
              (peer-port (usocket:get-peer-port new-socket)))
         (setf (usocket new-socket-obj) new-socket)
         (push peer-port (toss thread))
         (push (SOCK-calculate-int-ip-from-vec-ip peer-addr) (toss thread))
         (push (socket-id new-socket-obj) (toss thread))))
  (#\B bind-socket
       (let* ((addr (spop (toss thread)))
              (port (spop (toss thread)))
              (family (spop (toss thread)))
              (socket-id (spop (toss thread)))
              (socket (get-socket-from-socket-list socket-id)))
         (setf (local-addr socket) addr
               (local-port socket) port
               (addr-family socket) (cond ((= family 2)
                                           2)
                                          (t (error "unknown/not-implemented addr-family."))))))
  (#\C open-connection
       (let* ((addr (spop (toss thread)))
              (port (spop (toss thread)))
              (family (spop (toss thread)))
              (socket-id (spop (toss thread)))
              (socket (get-socket-from-socket-list socket-id)))
         (setf (remote-addr socket) addr
               (remote-port socket) port
               (addr-family socket) (cond ((= family 2)
                                           2)
                                          (t (error "unknown/not-implemented addr-family."))))
         (setf (usocket socket)
               (usocket:socket-connect (remote-addr socket)
                                       (remote-port socket)
                                       :protocol (socket-type socket)))))
  (#\K shutdown-socket
       (let* ((socket-id (spop (toss thread))))
         (remove-socket-from-socket-list socket-id)))
  (#\O set-socket-options
       (let* ((socket-id (spop (toss thread)))
              (option (spop (toss thread)))
              (value (spop (toss thread)))
              (socket (get-socket-from-socket-list socket-id)))
         (cond ((= option 2)
                (if (= value 0)
                    (setf (socket-option socket) nil)
                    (setf (socket-option socket) 2))))))
  (#\L set-backlog
       (let* ((socket-id (spop (toss thread)))
              (backlog (spop (toss thread)))
              (socket (get-socket-from-socket-list socket-id)))
         (setf (usocket socket)
               (usocket:socket-listen (local-addr socket)
                                      (local-port socket)
                                      :reuse-address (= (socket-option socket) 2)
                                      :backlog backlog))))
  (#\I convert-addr
       (push (SOCK-calculate-int-ip-from-vec-ip
              (usocket:dotted-quad-to-vector-quad
               (get-string-from-stack thread)))
             (toss thread)))
  (#\R read-data
       (let* ((socket-id (spop (toss thread)))
              (socket (get-socket-from-socket-list socket-id))
              (length (spop (toss thread)))
              (vec-buffer (get-vector-from-stack-absolute thread))
              (read-delta (let ((vec (make-n-list (dimensions (parent-funge-instance thread))
                                                  0)))
                            (setf (first vec) 1)
                            vec)))
         (push (loop
                  for n from 0 to (1- length)
                  for buf = vec-buffer then (setf buf (+-vector buf read-delta))
                  for char = (read-char (usocket:socket-stream (usocket socket))
                                        nil)
                  while char
                  do
                    (setf (cell* (parent-funge-instance thread) buf)
                          (char-code char))
                  finally (return n))
               (toss thread))))
  (#\W send-data
       (let* ((socket-id (spop (toss thread)))
              (socket (get-socket-from-socket-list socket-id))
              (length (spop (toss thread)))
              (vec-buffer (get-vector-from-stack-absolute thread))
              (read-delta (let ((vec (make-n-list (dimensions (parent-funge-instance thread))
                                                  0)))
                            (setf (first vec) 1)
                            vec)))
         
         (push (loop
                  for n from 0 to (1- length)
                  for buf = vec-buffer then (setf buf (+-vector buf read-delta))
                  do
                    (write-char (code-char (cell* (parent-funge-instance thread) buf ))
                                (usocket:socket-stream (usocket socket)))
                  finally (return n))
               (toss thread)))))

)










