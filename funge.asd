(in-package :cl-user)
(declaim (optimize (debug 0) (speed 3)))
(asdf:defsystem :funge
  :version "0.1"
  :depends-on (:usocket :log4cl :log4cl.log4slime)  ;; we need usocket for the SOCK-fingerprint
  :components ((:file "packages")
               (:file "vector-helpers" :depends-on ("packages"))
               (:file "funge-space" :depends-on ("vector-helpers"))
               (:file "playfields" :depends-on ("vector-helpers" "funge-space"))
               (:file "threads" :depends-on ("funge-space"))
               (:file "opcodes" :depends-on ("threads"))
               (:file "fingerprints" :depends-on ("threads"))))

