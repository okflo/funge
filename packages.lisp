(in-package :cl)

(defpackage :net.diesenbacher.funge
  (:use :cl)
  (:nicknames :funge)
  (:export :*funge-version*
           :start
           :dimensions
           :funge-instance
           :funge-space
           :funge-space-in-multiverse
           :kill-funge-space
           :least-coord
           :greatest-coord
           :playfield-corners
           :cell*
           :cell
           :current-directory
           ;; funge-space
           :recalc-borders
           :in-space-p
           :map-funge-space
           :copy-funge-space
           :fill-funge-space
           :safe-copy-funge-space
           :read-funge-native
           :read-funge-binary
           :write-funge-native
           :write-funge-binary
           :what-todo-when-finished
           ;; threads
           :thread
           :thread-counter
           :threads
           :instruction-pointer
           :tick
           :no-of-ticks
           :id
           :storage-offset
           :delta
           :string-mode
           :stack-stack
           :lifo
           ;; stuff from vertor-helpers
           :x
           :y
           :z
           :in-between
           :revers-vec
           :make-n-list
           :+-vector
           :--vector
           :*-vector-skalar
           ))


